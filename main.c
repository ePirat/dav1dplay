#include <stdio.h>
#include <assert.h>

#include <SDL2/SDL.h>

#include <dav1d/dav1d.h>

#include "demuxer/ivf.h"

char *file_path = NULL;
int should_terminate = 0;

typedef struct decoder_thread_context
{
    SDL_Renderer *renderer;

    // Lock protecting access to the texture
    SDL_mutex *lock;
    SDL_Texture *tex;

    // Timestamp of previous decoded frame
    Uint32 last_pts;
    // Timestamp of current decoded frame
    Uint32 current_pts;
    // Ticks when last frame was received
    Uint32 last_ticks;
    // PTS time base
    double timebase;
} dp_decoder_context_t;

/**
 * Destroy a dp_decoder_context_t
 *
 * \note  It is safe to call with a NULL pointer,
 *        in that case nothing is done.
 */
static void dp_dec_ctx_destroy(dp_decoder_context_t *dec_ctx)
{
    if (dec_ctx && dec_ctx->lock)
        SDL_DestroyMutex(dec_ctx->lock);
    free(dec_ctx);
}

/**
 * Create a dp_decoder_context_t
 *
 * \note  The dp_decoder_context_t must be destroyed
 *        again by using dp_dec_ctx_destroy.
 */
static dp_decoder_context_t *dp_dec_ctx_create(SDL_Renderer *renderer)
{
    dp_decoder_context_t *dec_ctx;

    assert(renderer != NULL);

    // Alloc
    dec_ctx = malloc(sizeof(dp_decoder_context_t));
    if (dec_ctx == NULL) {
        return NULL;
    }

    // Create Mutex
    dec_ctx->lock = SDL_CreateMutex();
    if (dec_ctx->lock == NULL) {
        fprintf(stderr, "SDL_CreateMutex failed: %s\n", SDL_GetError());
        dp_dec_ctx_destroy(dec_ctx);
        return NULL;
    }

    dec_ctx->tex = NULL;
    dec_ctx->renderer = renderer;
    dec_ctx->last_pts = 0;
    dec_ctx->last_ticks = 0;
    dec_ctx->current_pts = 0;
    dec_ctx->timebase = 0;

    return dec_ctx;
}

/**
 * Update the decoder context with a new dav1d picture
 *
 * Once the decoder decoded a new picture, this call can be used
 * to update the internal texture of the decoder context with the
 * new picture.
 */
static void dp_dec_ctx_update_with_dav1d_picture(dp_decoder_context_t *dec_ctx,
    Dav1dPicture *dav1d_pic)
{
    SDL_LockMutex(dec_ctx->lock);
    int width = dav1d_pic->p.w;
    int height = dav1d_pic->p.h;

    enum Dav1dPixelLayout dav1d_layout = dav1d_pic->p.layout;

    // FIXME: Handle this in a more graceful way with a nicer error message
    assert(DAV1D_PIXEL_LAYOUT_I420 == dav1d_layout);

    SDL_Texture *texture;
    // FIXME: Do not re-create a texture all the time
    texture = SDL_CreateTexture(dec_ctx->renderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, width, height);
    SDL_UpdateYUVTexture(texture, NULL,
        dav1d_pic->data[0], dav1d_pic->stride[0], // Y
        dav1d_pic->data[1], dav1d_pic->stride[1], // U
        dav1d_pic->data[2], dav1d_pic->stride[1]  // V
        );

    dec_ctx->current_pts = dav1d_pic->m.timestamp;

    dec_ctx->tex = texture;
    SDL_UnlockMutex(dec_ctx->lock);
}

/**
 * Render the currently available texture
 *
 * Renders the currently available texture, if any,
 * and destroys the texture after rendering.
 */
static void dp_dec_ctx_render(dp_decoder_context_t *dec_ctx)
{
    SDL_LockMutex(dec_ctx->lock);

    if (dec_ctx->tex == NULL) {
        SDL_UnlockMutex(dec_ctx->lock);
        return;
    }

    // Calculate time since last frame was received
    Uint32 ticks_now = SDL_GetTicks();
    Uint32 ticks_diff = (dec_ctx->last_ticks != 0) ? ticks_now - dec_ctx->last_ticks : 0;

    // Calculate when to display the frame
    Uint32 pts_diff = dec_ctx->current_pts - dec_ctx->last_pts;
    int32_t wait_time = (pts_diff * dec_ctx->timebase) * 1000 - ticks_diff;
    dec_ctx->last_pts = dec_ctx->current_pts;

    if (wait_time > 0) {
        SDL_Delay(wait_time);
    } else {
        fprintf(stderr, "Frame displayed %f seconds too late\n", wait_time/(float)1000);
    }

    dec_ctx->last_ticks = SDL_GetTicks();


    SDL_RenderClear(dec_ctx->renderer);
    SDL_RenderCopy(dec_ctx->renderer, dec_ctx->tex, NULL, NULL);
    SDL_RenderPresent(dec_ctx->renderer);

    SDL_DestroyTexture(dec_ctx->tex);
    dec_ctx->tex = NULL;

    SDL_UnlockMutex(dec_ctx->lock);
}

/* Decoder thread "main" function */
static int decoder_thread_main(void *cookie)
{
    dp_decoder_context_t *dec_ctx = cookie;

    Dav1dSettings lib_settings;
    Dav1dPicture p = { 0 };
    Dav1dContext *c;
    Dav1dData data;
    int res = 0;
    unsigned n_out = 0, total, timebase[2];

    // Stats
    Uint32 decoder_start = SDL_GetTicks();

    IvfInputContext in_ctx;

    if (ivf_open(&in_ctx, file_path, timebase, &total) < 0) {
        fprintf(stderr, "Failed to open demuxer\n");
        return 1;
    }

    double timebase_d = timebase[1]/(double)timebase[0];

    dec_ctx->timebase = timebase_d;

    dav1d_default_settings(&lib_settings);
    lib_settings.n_tile_threads = 2;
    lib_settings.n_frame_threads = 4;

    if ((res = dav1d_open(&c, &lib_settings))) {
        fprintf(stderr, "Failed opening dav1d decoder\n");
        return 1;
    }

    if ((res = ivf_read(&in_ctx, &data)) < 0) {
        ivf_close(&in_ctx);
        fprintf(stderr, "Failed demuxing input\n");
        return 1;
    }

    // Decoder loop
    do {
        if ((res = dav1d_send_data(c, &data)) < 0) {
            if (res != -EAGAIN) {
                dav1d_data_unref(&data);
                fprintf(stderr, "Error decoding frame: %s\n",
                        strerror(-res));
                break;
            }
        }

        if ((res = dav1d_get_picture(c, &p)) < 0) {
            if (res != -EAGAIN) {
                fprintf(stderr, "Error decoding frame: %s\n",
                        strerror(-res));
                break;
            }
            res = 0;
        } else {

            // Update texture with dav1d picture context
            dp_dec_ctx_update_with_dav1d_picture(dec_ctx, &p);
            dav1d_picture_unref(&p);

            n_out++;
        }
    } while ((data.sz > 0 || !ivf_read(&in_ctx, &data)) && !should_terminate);

    // Release remaining data
    if (data.sz > 0) dav1d_data_unref(&data);

    // Drain decoder
    do {
        res = dav1d_get_picture(c, &p);
        if (res < 0) {
            if (res != -EAGAIN) {
                fprintf(stderr, "Error decoding frame: %s\n",
                        strerror(-res));
                break;
            }
        } else {
            // Update texture with dav1d picture context
            dp_dec_ctx_update_with_dav1d_picture(dec_ctx, &p);
            dav1d_picture_unref(&p);

            n_out++;
        }
    } while (res != -EAGAIN && !should_terminate);

    ivf_close(&in_ctx);
    dav1d_close(&c);

    // Stats
    Uint32 decoding_time_ms = SDL_GetTicks() - decoder_start;

    printf("Decoded %u frames in %d seconds, avg %.02f fps\n", n_out, decoding_time_ms/1000, n_out / (decoding_time_ms / 1000.0));
    return (res != -EAGAIN && res < 0);
}

int main(int argc, char **argv)
{
    SDL_Thread *decoder_thread;

    SDL_Window *win = NULL;
    SDL_Renderer *renderer = NULL;

    if (argc != 2) {
        fprintf(stderr, "Usage: dav1dplay <INPUTFILE>\n");
        return 1;
    }
    file_path = argv[1];

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
        return 10;
    
    // Create Window and Renderer
    win = SDL_CreateWindow("Dav1dPlay", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        910, 512, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_SetWindowResizable(win, SDL_TRUE);
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    // Start decoder thread
    dp_decoder_context_t *dec_ctx = dp_dec_ctx_create(renderer);
    if (dec_ctx == NULL) {
        fprintf(stderr, "Failed creating decoder context\n");
        return 5;
    }

    decoder_thread = SDL_CreateThread(decoder_thread_main, "Decoder thread", dec_ctx);

    // Main loop
    while (1) {

        SDL_Event e;
        if ( SDL_PollEvent(&e) ) {
            if (e.type == SDL_QUIT) {
                should_terminate = 1;
                break;
            }
        }

        dp_dec_ctx_render(dec_ctx);
    }

    int decoder_ret = 0;
    SDL_WaitThread(decoder_thread, &decoder_ret);

    dp_dec_ctx_destroy(dec_ctx);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);
    
    return decoder_ret;
}
