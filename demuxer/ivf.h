//
//  ivf.h
//  PlayAV1
//
//  Created by Marvin Scholz on 16.12.18.
//  Copyright © 2018 ePirat. All rights reserved.
//

#ifndef ivf_h
#define ivf_h

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <dav1d/dav1d.h>

typedef struct DemuxerPriv {
    FILE *f;
} IvfInputContext;

int ivf_open(IvfInputContext *const c, const char *const file,
                    unsigned fps[2], unsigned *const num_frames);

int ivf_read(IvfInputContext *const c, Dav1dData *const buf);

void ivf_close(IvfInputContext *const c);

#endif /* ivf_h */
